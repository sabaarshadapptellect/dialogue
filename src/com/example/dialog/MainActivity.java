package com.example.dialog;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		function();
	}

	private void function() {
		AlertDialog alertDialog = new AlertDialog.Builder(
				MainActivity.this).create();

		// Setting Dialog Title
		alertDialog.setTitle("Medical App");

		// Setting Dialog Message
		alertDialog.setMessage("Slot booked sucessfully");

		// Setting Icon to Dialog
		//alertDialog.setIcon(R.drawable.tick);

		// Setting OK Button
		alertDialog.setButton("OK",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,int which) {
						// Write your code here to execute after dialog
						// closed
						Toast.makeText(getApplicationContext(),
								"You clicked on OK", Toast.LENGTH_SHORT)
								.show();
					}
				});
		

		// Showing Alert Message
		alertDialog.show();

	}
		
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
